/**
 * Created by sta7ic on 9/21/2015.
 *
 * This file contains methods and commands to generate non-static content for the ProbeHelper page.
 */

function Part(pName, pLabel, mass, cost, power, energy, data, xmit){
    this.pName = pName;
    this.pLabel = pLabel;
    this.mass = mass;
    this.cost = cost;
    this.power = power;
    this.energy = energy;
    this.data = data;
    this.xmit = xmit;
    //console.log('built part with pName '+this.pName);
}

function Spob(name, sma, start){
    this.name = name;
    this.sma = sma;
    this.start = start;
}

var spobs = [
    new Spob('Moho', 5263138304, false),
    new Spob('Eve', 9832684544, false),
    new Spob('Gilly', 9832684544+31500000, false),
    new Spob('Kerbin', 13599840256, true),
    new Spob('Mun', 13599840256+12000000, false),
    new Spob('Minmus', 13599840256+47000000, false),
    new Spob('Duna', 20726155264, false),
    new Spob('Ike', 20726155264+3200000, false),
    new Spob('Dres', 40839348203, false),
    new Spob('Jool', 68773560320, false),
    new Spob('Laythe', 68773560320+27184000, false),
    new Spob('Vall', 68773560320+43152000, false),
    new Spob('Tylo', 68773560320+68500000, false),
    new Spob('Bop', 68773560320+128500000, false),
    new Spob('Pol', 68773560320+179890000, false),
    new Spob('Eeloo', 90118820000, false)
];

var kerbin = spobs[3];

function getSolarRatio(){
    var destSpobCtl = $('#destinationSpob');
    var destSpobId = destSpobCtl.val();
    var destSpob = spobs[destSpobId];
    //var useSma;
    if(destSpob.sma < kerbin.sma) {
        //useSma = kerbin.sma;
        return 1.0;
    } else {
        //useSma = destSpob.sma;
        var ratio = kerbin.sma / destSpob.sma;
        return (ratio * ratio);
    }
}

function addSpobSelect(selHndl){
    $.each(spobs, function(k, v){
        if(v.start == true)
            selHndl.append($('<option>', {value : k, selected : 'true'}).text(v.name));
        else
            selHndl.append($('<option>', {value : k}).text(v.name));
    });
}

function addParts(p1, p2){
    var pn = new Part('', '', '', 0, 0, 0, 0, 0, 0);
    pn.mass = p1.mass + p2.mass;
    pn.cost = p1.cost + p2.cost;
    pn.power = p1.power + p2.power;
    pn.energy = p1.energy + p2.energy;
    pn.data = p1.data + p2.data;
    pn.xmit = p1.xmit + p2.xmit;
    return pn;
}
//function foo(pName){this.pName = pName;}
//var bar = foo('Stan');
//console.log('Get in the car, '+bar.pName);

function writeRadios(theDiv, inHead, inName, inClass, parts){
    theDiv.innerHTML = '<h5>'+inHead+'</h5>';
    for(i = 0; i < parts.length; ++i){
        var add = '<p><input type="radio" name="'+inName+'" class="'+inClass+'" value="';
        add += i;
        add += '">';
        add += parts[i].pLabel;
        add += '</input></p>';
        theDiv.innerHTML += add;
    }
}

var cores = [
    new Part('sputnik', 'Stayputnik', 0.05, 300, 1.67/60.0, 10, 0, 0),
    new Part('cube', 'QBE', 0.07, 360, 1.50/60.0, 5.0, 0, 0),
    new Part('okto1', 'OKTO', 0.1, 450, 1.2/60, 10, 0, 0),
    new Part('hecs', 'HECS', 0.1, 650, 1.5/60.0, 10.0, 0, 0),
    new Part('okto2', 'OKTO2', 0.04, 1480, 1.8/60, 5, 0, 0),
    new Part('hecs2', 'HECS2', 0.2, 4500, 3.0/60, 1000, 0, 0)
];

function addCores(cores) {
    var theDiv = document.getElementById('payloadCore');
    writeRadios(theDiv, 'Probe Cores', 'probeCore', 'probeInput', cores);
}

var flatSolar = [
    new Part('oxstat', 'OX-STAT PV Panel', 0.005, 75, 0.35, 0, 0, 0),
    new Part('oxstatxl', 'OX-STAT-XL PV Panel', 0.04, 600, 2.80, 0, 0, 0)
];

var fixedSolar = [
    new Part('ox4', 'OX-4 PV Panel', 0.0175, 380, 1.64, 0, 0, 0),
    new Part('gigantor', 'Gigantor XL Solary Array', 0.3, 3000, 24.4, 0, 0, 0),
];

var flexSolar = [
    new Part('spw', 'SP-X PV Panel', 0.025, 440, 1.64, 0, 0, 0),
    new Part('gigantor', 'Gigantor XL Solary Array', 0.3, 3000, 24.4, 0, 0, 0),
];

var theRtg = new Part('rtg', 'PB-NUK RTG', 0.08, 23300, 0.75, 0, 0, 0);
var fuelCell = new Part('fuelCell', 'Fuel Cell', 0.05, 750, 1.5, 50, 0, 0);

function findSolarParts(powerNeed, rechargeRate, solarFactor, parts, scale){
    var effNeed = powerNeed/solarFactor;
    var effWant = rechargeRate/solarFactor;
    // pick the greater requirement: user-specified recharge time OR minimum to operate
    if(effWant > effNeed)
        effNeed = effWant;
    var res = new Object();
    // do we need enough to justify gigantors?
    var giantPair = parts[1].power * scale;
    if(effNeed > giantPair){
        var giantNeed = Math.ceil(effNeed / giantPair);
        //console.log('need = '+effNeed / giantPair);
        res.count = giantNeed * scale;
        res.type = parts[1];
    } else {
        var smallPair = parts[0].power * scale;
        //console.log('need = '+effNeed / smallPair);
        var smallNeed = Math.ceil(effNeed / smallPair);
        res.count = smallNeed * scale;
        res.type = parts[0];
    }
    return res;
}

function calcPowerParts(powerNeed, rechargeRate, solarFactor){
    //console.log('calcPowerParts: '+powerNeed+', '+rechargeRate+', '+solarFactor);
    var solarMode = $('#solarSelect').val();
    //console.log('solarMode = '+solarMode);
    var solarPart = new Part('', '', 0, 0, 0, 0, 0);
    var solarRes = undefined;
    switch(solarMode){
        case '0': // no solar
            break;
        case '1': // flat panels
            // assume 6x and 1 0deg + 2x 60deg panels (half effect each)
            solarFactor /= 3.0; // assume we need six panels to do the work of two
            solarRes = findSolarParts(powerNeed, rechargeRate, solarFactor, flatSolar, 6);
            break;
        case '2': // space panels
            solarRes = findSolarParts(powerNeed, rechargeRate, solarFactor, fixedSolar, 2);
            break;
        case '3': // folding panels
            solarRes = findSolarParts(powerNeed, rechargeRate, solarFactor, flexSolar, 2);
            break;
    }
    if(solarRes != undefined){
        //console.log('solarRes: part = '+solarRes.type.pName+', ct = '+solarRes.count);
        solarPart = new Part('', '', solarRes.type.mass * solarRes.count, solarRes.type.cost * solarRes.count, solarRes.type.power * solarRes.count * solarFactor, 0, 0, 0);
        $('#solarPickSpan').text(solarRes.count.toFixed(0).toString()+'x '+solarRes.type.pLabel);
    }
    return solarPart;
}

var batteries = [
    new Part('z100', 'Z-100 Rechargable Radial', 0.005, 80, 0, 100, 0, 0),
    new Part('z200', 'Z-200 Rechargable Inline', 0.01, 360, 0, 200, 0, 0),
    new Part('z400', 'Z-400 Rechargable Radial', 0.02, 550, 0, 400, 0, 0),
    new Part('z1000', 'Z-1000 Rechargable Inline', 0.1, 880, 0, 1000, 0, 0)
];

function addBatteries(){
    // add the 'batteries' names to the select field '#batterySelect'
    var battHndl = $('#batterySelect');
    $.each(batteries, function(k, v){
        battHndl.append($('<option>', {value : k}).text(v.pLabel));
    });
}

function calcBatteryNeed(energyNeed){
    if(energyNeed <= 0.0){
        $('#batteryPickSpan').text('');
        return new Part('', '', 0, 0, 0, 0, 0);
    }
    var battPick = $('#batterySelect').val();
    var theBatt = batteries[battPick];
    var howMany = Math.ceil(energyNeed / theBatt.energy);
    $('#batteryPickSpan').text(howMany.toFixed(0).toString()+'x '+theBatt.pLabel);
    return new Part('', '', theBatt.mass * howMany, theBatt.cost * howMany, 0, theBatt.energy * howMany, 0, 0);
}

// power = E/Mit, xmit = Mit/s
var comms = [
    new Part('whip', 'Communotron 16', 0.01, 300, 6.0, 0, 0, 3.33),
    new Part('panel', 'Comms DTS-M1', 0.03, 600, 6.0, 0, 0, 5.71),
    new Part('dish', 'Communotron 88-88', 0.03, 1100, 10.0, 0, 0, 20.0)
];

function addComms(){
    var theDiv = document.getElementById('payloadComm');
    writeRadios(theDiv, 'Probe Antenna', 'probeComm', 'probeInput', comms);
}

var sensors = [
    new Part('scijr', 'SC-9001 Science Jr', 0.2, 880, 0, 0, 25, 0),
    new Part('goo', 'Mystery Goo Containment Unit (pair)', 0.10, 1600, 0, 0, 10),
    new Part('accelerometer', 'Double-C Seismic Accelerometer', 0.01, 6000, 0.0075, 0, 50, 0),
    new Part('barometer', 'PresMat Barometer', 0.01, 3300, 0.0075, 0, 12, 0),
    new Part('gravometer', 'GRAVMAX Negative Gravioli Detector', 0.01, 8800, 0.0075, 0, 60, 0),
    new Part('thermometer', '2HOT Thermometer', 0.01, 900, 0.0075, 0, 8, 0),
    new Part('atmoscan', 'Atmospheric Fluid Spectro-Variometer', 0.01, 6500, 0, 0, 200, 0),
    new Part('survey', 'M700 Survey Scanner', 0.2, 1500, 0, 0, 0, 0), // TODO identify survey scan energy needs
    new Part('narrowsurvey', 'M4435 Narrow-Band Scanner', 0.1, 1000, 0, 0, 0, 0),
    new Part('surfaceScan', 'Surface Scanning Module', 0.01, 800, 0, 0, 0, 0)
];

function writeCheckers(theDiv, inHead, inName, inClass, parts){
    theDiv.innerHTML = '<h5>'+inHead+'</h5>';
    for(i = 0; i < parts.length; ++i){
        var add = '<p><input type="checkbox" name="'+inName+'" class="'+inClass+'" value="';
        add += i;
        add += '">';
        add += parts[i].pLabel;
        add += '</input></p>';
        theDiv.innerHTML += add;
    }
}

function addScience(){
    var theDiv = document.getElementById('payloadScience');
    writeCheckers(theDiv, 'Scientific Instruments', 'probeSensor', 'probeInput', sensors);
}

function updateSummary(m, c, p, pN, bC, bN, bS, r, dS, dM, dT) {
    $('#massSpan').text(m.toFixed(3).toString() + ' tons');
    $('#costSpan').text(c.toString() + ' funds');
    $('#powerSpan').text(p.toFixed(3).toString() + ' E/sec');
    $('#powerNeedSpan').text(pN.toFixed(3).toString() + ' E/sec');
    $('#batteryCapSpan').text(bC.toFixed(3).toString() + ' E');
    $('#batteryNeedSpan').text(bN.toFixed(3).toString() + ' E');
    $('#batteryStoreSpan').text(bS.toFixed(3).toString() + ' E');
    $('#rechargeTimeSpan').text(r.toFixed(3).toString() + ' sec');
    $('#dataMaxSpan').text(dM.toFixed(3).toString() + ' Mits');
    $('#dataTotalSpan').text(dS.toFixed(3).toString() + ' Mits');
    $('#dataTimeSpan').text(dT.toFixed(3).toString() + ' sec');
}

function calcCommNeed(dataSum, dataMax, commPart) {
    var commMode = $('#transModeSel').val();
    var power = commPart.power;
    switch(commMode){
        case '0':
            return 0.0;
            break;
        case '1':
            return power * dataMax;
            break;
        case '2':
            return power * dataSum;
            break;
    };
}

function calcCommTime(dataSum, dataMax, commPart) {
    var commMode = $('#transModeSel').val();
    var rate = commPart.xmit;
    if(rate == 0.0 || rate == undefined)
        return 0;
    switch(commMode){
        case '0':
            return 0.0;
            break;
        case '1':
            return dataMax / rate;
            break;
        case '2':
            return dataSum / rate;
            break;
    };
}

function updatePower(){
    var rv = new Part('', '', '', 0, 0, 0, 0, 0);

    var solarScale = getSolarRatio();
    $('#solarFactorSpan').text(solarScale.toPrecision(4));
    return solarScale;
}

function doUpdateSummary(){
    /* $( "div" ).text( str ); */
    var mass = 0.0;
    var cost = 0.0;
    var power = 0.0;
    var powerNeed = 0.0;
    var battCap = 0.0;
    var battNeed = 0.0;
    var recharge = 0.0;
    var dataSum = 0;
    var dataMax = 0;
    var transmitTime = 0;

    /* $("#myform input[name='radioName']:checked").val(); */
    var coreId = $('#probeHelperForm input[name="probeCore"]:checked').val();
    var powerId = $('#probeHelperForm input[name="probePower"]:checked').val();
    var commId = $('#probeHelperForm input[name="probeComm"]:checked').val();

    /* get, accumulate science selected here */

    var corePart = undefined;
    var powerPart = undefined;
    var commPart = undefined;

    var zeroPart = new Part('', '', '', 0, 0, 0, 0, 0, 0);
    if(coreId == undefined)
        corePart = zeroPart;
    else
        corePart = cores[coreId];

    if(powerId == undefined)
        powerPart = zeroPart;
    else
        powerPart = zeroPart; // need to come up with this, since solar varies. -MH

    var solarScale = updatePower();

    if(commId == undefined)
        commPart = zeroPart;
    else
        commPart = comms[commId];

    // get sensors
    var sensorArray = new Array();
    $("#probeHelperForm input:checkbox[name=probeSensor]:checked").each(function(){
        sensorArray.push($(this).val());
    });

    var sensorPart = new Part('', '', 0, 0, 0, 0, 0, 0);
    for(var i = 0; i < sensorArray.length; ++i){
        var thisPart = sensors[sensorArray[i]];
        sensorPart = addParts(sensorPart, thisPart);
        if(thisPart.data > dataMax) {
            dataMax = thisPart.data;
        }
    }

    var structMass = parseFloat($('#structMass').val());

    power = 0.0; // solar + RTG, at Kerbin and destination
    powerNeed = Math.abs(corePart.power + sensorPart.power);

    var coreBattNeed = parseFloat($("#shadowTime").val()) * powerNeed;

    dataSum = sensorPart.data;
    var commNeed = calcCommNeed(dataSum, dataMax, commPart);
    transmitTime = calcCommTime(dataSum, dataMax, commPart);

    // pick batteries
    battCap = corePart.energy;
    battNeed = coreBattNeed + commNeed;
    var battPart = calcBatteryNeed(battNeed - corePart.energy);
    var realBatt = battPart.energy + corePart.energy;

    // calculate recharge time
    recharge = 0.0;
    var rechargeReq = $('#rechargeTargTime').val();
    var powerPart = calcPowerParts(powerNeed, realBatt/rechargeReq, solarScale);
    power = powerPart.power;
    recharge = realBatt / power;

    // finally
    mass = cores[coreId].mass + comms[commId].mass + sensorPart.mass + battPart.mass + powerPart.mass + structMass;
    cost = corePart.cost + commPart.cost + sensorPart.cost + battPart.cost + powerPart.cost;

    /* recalculate here */
    updateSummary(mass, cost, power, powerNeed, battCap, battNeed, realBatt, recharge, dataSum, dataMax, transmitTime);
}


var main = function(){
    addCores(cores);    // add probe cores (DDM?)
    //addPower();         // add power options
    addBatteries();
    addComms();         // add antennas (radio)
    addScience();       // add science options
    addSpobSelect($('#originSpob'));
    addSpobSelect($('#destinationSpob'));
    $('.probeInput input').change(function(){
        doUpdateSummary();
    });
    $('.probeInput select').change(function(){
        doUpdateSummary();
    });
    $('#probeHelperForm input[value="0"]:radio').prop('checked', 'true'); // check all inputs in probeHelperForm where value==0

    doUpdateSummary();
};

$(document).ready(main);